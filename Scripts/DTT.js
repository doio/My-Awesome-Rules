/*
DTT

***************************
QuantumultX:

[rewrite_local]
^https?:\/\/metroman\.oss-cn-hangzhou\.aliyuncs\.com\/app\/metromanios\/v+\d{6}\/config+\d{8}\.json url script-response-body DTT.js

[mitm]
hostname = metroman.oss-cn-hangzhou.aliyuncs.com

***************************
Surge4 or Loon:

[Script]
http-response ^https?:\/\/metroman\.oss-cn-hangzhou\.aliyuncs\.com\/app\/metromanios\/v+\d{6}\/config+\d{8}\.json requires-body=1,max-size=0,script-path=DTT.js

[MITM]
hostname = metroman.oss-cn-hangzhou.aliyuncs.com

**************************/

var obj = JSON.parse($response.body);
obj.0.vip = 1;
obj.0.tencent = 0;
obj.0.bytedance = 0;
obj.0.google = 0;
obj.1.vip = 1;
obj.1.tencent = 0;
obj.1.bytedance = 0;
obj.1.google = 0;
obj.vip = 1;
obj.tencent = 0;
obj.bytedance = 0;
obj.google = 0;
$done({body: JSON.stringify(obj)}); 
