/* test filter api */
const localStorage = {};

function testFilterResponse(type, id, url, data, response) {
    var parser = localStorage[id] || (localStorage[id] = new HTTPParser(type));
    parser.onHeadersComplete = function(info) {
        console.log('onHeadersComplete', info);
    };
    parser.onBody = function(chunk, offset, length) {
        console.log('onBody', chunk, offset, length);
    };
    parser.onMessageComplete = function() {
        console.log('onMessageComplete');
    };
    parser.execute(data);
}

function testMockResponse(type, id, url, data, response) {
    if (!localStorage[id]) {
        localStorage[id] = {}
    }
    var parser = localStorage[id][type];
    if (!parser) {
        parser = localStorage[id][type] = new HTTPParser(type);
        parser.onHeadersComplete = function(info) {
            console.log('onHeadersComplete', info);
        };
        parser.onBody = function(chunk, offset, length) {
            console.log('onBody =>', chunk, offset, length);
        };
        parser.onMessageComplete = function() {
            console.log('onMessageComplete');
        };
    }
    parser.execute(data);
    console.log(parser.info);
    var time = new Date().toGMTString();
    response('HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: ' + time.length + '\r\n\r\n' + time);
}

/**
 * Filter HTTP request and response.
 * @param {string}: http type, REQUEST or RESPONSE
 * @param {string}: http request UUID
 * @param {string}: http request url
 * @param {string}: http raw data
 * @param {function}: function(data){}
 * @returns {string|boolean|undefined} string:return modified data, boolean:close connection, undefined:do not modify data
 */
function scriptFilterExecute(type, id, url, data, response) {
    console.log('filter ' + id);
    console.log('url ' + url);
    if (url.indexOf('http://baidu.com/robots.txt?') === 0) {
        if (type == 'RESPONSE') {
            testFilterResponse(type, id, url, data, response);
        }
    } else if (url.indexOf('http://baidu.com/robots.txt') === 0) {
        testMockResponse(type, id, url, data, response);
        return true;
    }
}

/*
 * Release filter values.
 * @param {string}: http request UUID
 */
function scriptFilterFree(id) {
    console.log('free ' + id);
    delete localStorage[id];
}