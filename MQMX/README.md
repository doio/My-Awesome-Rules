### Surge-related 配置片段

#### 策略组正则过滤公式
<pre>
正则过滤基本公式
(A).*(B)             节点名既有 A 也有 B 
(A)|(B)              节点名有 A 或者 B   
^(?!.*A)             节点名不含有 A 
^(?!.*A).*(B)        节点名不含有 A 但含有 B
^(?!.*(A|B))         节点名既不含有 A 也不含有 B
</pre>

#### 策略组编写
```conf
香港节点 = smart, update-interval=0, policy-regex-filter=(?i)🇭🇰|香港|(\b(HK|Hong)\b), no-alert=1, hidden=1, include-all-proxies=0, include-other-group=手动选择, interval=300, tolerance=0
美国节点 = smart, update-interval=0, policy-regex-filter=(?i)🇺🇸|美国|洛杉矶|圣何塞|(\b(US|United States)\b), no-alert=1, hidden=1, include-all-proxies=0, include-other-group=手动选择, interval=300, tolerance=0
狮城节点 = smart, update-interval=0, policy-regex-filter=(?i)🇸🇬|新加坡|狮|(\b(SG|Singapore)\b), no-alert=1, hidden=1, include-all-proxies=0, include-other-group=手动选择, interval=300, tolerance=0
日本节点 = smart, update-interval=0, policy-regex-filter=(?i)🇯🇵|日本|东京|(\b(JP|Japan)\b), no-alert=1, hidden=1, include-all-proxies=0, include-other-group=手动选择, interval=300, tolerance=0, persistent=0
台湾节点 = smart, update-interval=0, policy-regex-filter=(?i)🇨🇳|🇹🇼|台湾|(\b(TW|Tai|Taiwan)\b), no-alert=1, hidden=1, include-all-proxies=0, include-other-group=手动选择, interval=300, tolerance=0
```
```conf
🇭🇰 香港节点 = smart, include-other-group=🚀 我的节点, update-interval=0, no-alert=0, hidden=0, include-all-proxies=0, policy-regex-filter=(🇭🇰)|(港)|(Hong)|(HK)
🇨🇳 台湾节点 = smart, include-other-group=🚀 我的节点, update-interval=0, no-alert=0, hidden=0, include-all-proxies=0, policy-regex-filter=(🇨🇳)|(台)|(Tai)|(TW)
🇺🇲 美国节点 = smart, include-other-group=🚀 我的节点, update-interval=0, no-alert=0, hidden=0, include-all-proxies=0, policy-regex-filter=(🇺🇸)|(美)|(States)|(US)
🇯🇵 日本节点 = smart, include-other-group=🚀 我的节点, update-interval=0, no-alert=0, hidden=0, include-all-proxies=0, policy-regex-filter=(🇯🇵)|(日)|(Japan)|(JP)
🇸🇬 新加坡节点 = smart, include-other-group=🚀 我的节点, update-interval=0, no-alert=0, hidden=0, include-all-proxies=0, policy-regex-filter=(🇸🇬)|(新)|(Singapore)|(SG)
🚀 我的节点 = select, policy-path=你的订阅地址, update-interval=0, no-alert=0, hidden=0, include-all-proxies=0
```
